`include "dff.v"

module tb_dff();

  reg rst;
  reg clk;
  reg en;
  reg d;

  wire q;
  wire qz;

  assign clken = clk | en;
  
  dff U_dff
  (
    .rst (rst),
    .clk (clken),
    .d (d),
    .q (q),
    .qz (qz)
  );

  wire fdq;
  wire fdqz;

  dff U_freqdiv
  (
    .rst (rst),
    .clk (clken),
    .d (fdqz),
    .q (fdq),
    .qz (fdqz)
  );

  initial
  begin
    rst <= 0;
    clk <= 0;
    en <= 0;
    d <= 1;

    #5 rst = 1;
    #50 rst = 0;

    #20 d = 0;
    #10 d = 1;

    #10 d = 0;
    #10 d = 1;

    #10 d = 0;
    #30 d = 1;

    #50 $finish;
  end

  always
  begin
    #10 clk = ~clk;
  end

  initial
  begin
    $dumpfile("out/tb_dff.lxt");
    $dumpvars(0,tb_dff);
  end

endmodule
