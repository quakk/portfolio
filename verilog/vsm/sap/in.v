/*
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

module in
(
  enable,
  data_in,  
  data_out
);

  input enable;
  input [3:0] data_in;
  output [3:0] data_out; 

  wire enable;
  wire [3:0] data_in;
  wire [3:0] data_out;

  assign data_out = (enable) ? data_in : 4'bz;

endmodule
