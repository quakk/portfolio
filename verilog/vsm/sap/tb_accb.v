`include "accb.v"

module tb_accb();

  reg rst;
  reg load_b;
  reg clk;
  reg [3:0] data;

  wire [3:0] alu;

  accb U_accb
  (
    .rst (rst),
    .clk (clk),
    .load_b (load_b),
    .b_in (data),
    .alu_out (alu)
  );

  initial
  begin
    rst <= 1;
    clk <= 0;
    load_b <= 0;
    data = 4'b0;

    #5 rst = 0;
    #50 rst = 1;

    #15 data = 4'h5;
    load_b = 1;

    #10 load_b = 0;
    #10 data = 4'h0;

    #20 data = 4'hc;
    load_b = 1;

    #10 load_b = 0;
    #10 data = 4'h0;

    #50 $finish;
  end

  always
  begin
    #10 clk = ~clk;
  end

  initial
  begin
    $dumpfile("out/tb_accb.lxt");
    $dumpvars(0,tb_accb);
  end

endmodule  
