`include "pc.v"

module tb_pc();

  reg pc_rst;
  reg pc_clk;
  reg pc_en;
  wire [3:0] pc_count;

  pc U_pc
  (
    .rst (pc_rst),
    .clk (pc_clk),
    .en (pc_en),
    .count (pc_count)
  );

  initial
  begin
    pc_rst <= 0;
    pc_clk <= 0;
    pc_en <= 0;

    #50 pc_rst <= 0;
    #500 pc_rst = 1;

    #100 pc_en <= 1;
    #600;

    #50 $finish;
  end

  always
  begin
    #10 pc_clk = ~pc_clk;
  end

  initial
  begin
    $dumpfile("out/tb_pc.lxt");
    $dumpvars(0,tb_pc);
  end

endmodule
