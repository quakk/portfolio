`include "in.v"

module tb_in();

  reg enable_in;
  reg [3:0] data_in;
  wire [3:0] data_out;

  in U_in
  (
    .enable (enable_in),
    .data_in (data_in),
    .data_out (data_out)
  );

  initial
  begin
    enable_in <= 0;
    data_in <= 4'h0;

    #20 enable_in <= 1;
    #20 data_in = 4'h1;
    #20 data_in = 4'h3;
    #20 data_in = 4'h4;
    #20 data_in = 4'h5;
    #20 data_in = 4'h6;
    #20 data_in = 4'h7;
    #20 data_in = 4'h8;
    #20 data_in = 4'h9;
    #20 data_in = 4'ha;
    #20 data_in = 4'hb;
    #20 data_in = 4'hc;
    #20 data_in = 4'hd;
    #20 data_in = 4'he;
    #20 data_in = 4'hf;

    #20 data_in = 4'h0;
    #20 enable_in <= 0;
    #20 data_in = 4'h1;
    #20 data_in = 4'h2;
    #20 data_in = 4'h3;
    #20 data_in = 4'h4;
    #20 data_in = 4'h5;
    #20 data_in = 4'h6;
    #20 data_in = 4'h7;
    #20 data_in = 4'h8;
    #20 data_in = 4'h9;
    #20 data_in = 4'ha;
    #20 data_in = 4'hb;
    #20 data_in = 4'hc;
    #20 data_in = 4'hd;
    #20 data_in = 4'he;
    #20 data_in = 4'hf;


    #50 $finish;

    #50 $finish;
  end

  initial
  begin
    $dumpfile("out/tb_in.lxt");
    $dumpvars(0,tb_in);
  end

endmodule
