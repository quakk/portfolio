`include "mem.v"

module tb_mem();

  reg readmem;
  reg writemem;
  reg [2:0] addr;
  reg [7:0] data_in;
  wire [7:0] data_out;

  memory U_mem
  (
    .read (readmem),
    .write (writemem),
    .addr (addr),
    .data_in (data_in),
    .data_out (data_out)
  );

  initial
  begin
    readmem = 0;
    writemem = 0;
    addr = 3'b000;
    data_in = 8'h0;

    #20 writemem = 1;
    #20 addr = 3'b001;
    data_in = 8'h1;
    #20 addr = 3'b010;
    data_in = 8'h2;
    #20 addr = 3'b011;
    data_in = 8'h3;
    #20 addr = 3'b100;
    data_in = 8'h4;
    #20 addr = 3'b101;
    data_in = 8'h5;
    #20 addr = 3'b110;
    data_in = 8'h6;
    #20 addr = 3'b111;
    data_in = 8'h7;
    #20 writemem = 0;

    #20 addr = 3'b000;
    #20 addr = 3'b001;
    #20 addr = 3'b010;
    #20 addr = 3'b011;
    #20 addr = 3'b100;
    #20 addr = 3'b101;
    #20 addr = 3'b110;
    #20 addr = 3'b111;

    #20 addr = 3'b000;
    readmem = 1;
    #20 addr = 3'b001;
    #20 addr = 3'b010;
    #20 addr = 3'b011;
    #20 addr = 3'b100;
    #20 addr = 3'b101;
    #20 addr = 3'b110;
    #20 addr = 3'b111;
    #20 readmem = 0;
    
    #20 addr = 3'b000;
    data_in = 8'h7;
    #20 writemem = 1;
    #20 addr = 3'b001;
    data_in = 8'h6;
    #20 addr = 3'b010;
    data_in = 8'h5;
    #20 addr = 3'b011;
    data_in = 8'h4;
    #20 addr = 3'b100;
    data_in = 8'h3;
    #20 addr = 3'b101;
    data_in = 8'h2;
    #20 addr = 3'b110;
    data_in = 8'h1;
    #20 addr = 3'b111;
    data_in = 8'h0;
    #20 writemem = 0;

    #20 addr = 3'b000;
    readmem = 1;
    #20 addr = 3'b001;
    #20 addr = 3'b010;
    #20 addr = 3'b011;
    #20 addr = 3'b100;
    #20 addr = 3'b101;
    #20 addr = 3'b110;
    #20 addr = 3'b111;
    #20 readmem = 0;

    #50 $finish;
  end

  initial
  begin
    $dumpfile("out/tb_mem.lxt");
    $dumpvars(0,tb_mem);
  end

endmodule  
