`include "mux.v"

module tb_mux();

  reg in_0;
  reg in_1;
  reg sel;
  wire out;

  mux U_mux
  (
    .in_0 (in_0),
    .in_1 (in_1),
    .sel (sel),
    .out (out)
  );

  initial
  begin
    in_0 = 0;
    in_1 <= 0;
    sel <= 0;

    #20 in_0 = 1;
    #20 in_1 = 1;
    in_0 = 0;
    #20 in_0 = 1;
    #20 sel = 1;
    in_0 = 0;
    in_1 = 0;
    #20 in_0 = 1;
    #20 in_1 = 1;
    in_0 = 0;
    #20 in_0 = 1;

    #50 $finish;
  end

  initial
  begin
    $dumpfile("out/tb_mux.lxt");
    $dumpvars(0,tb_mux);
  end

endmodule
