module adder
(
  a,
  b,
  ci,
  s,
  co
);

  input a;
  input b;
  input ci;
  output s;
  output co;

  wire a;
  wire b;
  wire ci;
  wire s;
  wire co;

  assign s = (a ^ b) ^ ci;
  assign co = (a & b) | (ci & (a | b));

endmodule
