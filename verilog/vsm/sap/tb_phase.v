`include "phase.v"

module tb_phase();

  reg phase_rstz;
  reg phase_clk;
  wire [3:0] phase_phase;

  phase U_phase
  (
    .rstz (phase_rstz),
    .clk (phase_clk),
    .phase (phase_phase)
  );

  initial
  begin
    phase_rstz <= 0;
    phase_clk <= 0;

    #50 phase_rstz = 1;

    #500 $finish;
  end

  always
  begin
    #10 phase_clk = ~phase_clk;
  end

  initial
  begin
    $dumpfile("out/tb_phase.lxt");
    $dumpvars(0,tb_phase);
  end

endmodule
