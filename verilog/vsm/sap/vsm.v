`include "phase.v"
`include "control.v"
`include "pc.v"
`include "mem.v"
`include "ir.v"
`include "accb.v"
`include "acca.v"
`include "alu.v"
`include "in.v"
`include "out.v"

module vsm(

  // asynchronous reset
  rst,

  // source clock
  clk,

  // for loading internal memory
  writemem,
  memin,

  // input register
  din,

  // internal bus
  bus,

  // result produced by microprocessor
  out

);

  input rst;
  input clk;
  input writemem;
  input [7:0] memin;
  input [3:0] din;
  output [3:0] bus;
  output [3:0] out;

  wire rst;
  wire clk;
  wire writemem;
  wire [7:0] memin;
  wire [3:0] din;

  // these are driven by internal components
  wire [3:0] bus;
  wire [3:0] out;

  // 4-phase clock
  // driven by phase
  wire [3:0] phaseclk;

  // instruction
  // driven by ir
  wire [3:0] instruction;

  // control signals
  // driven by control
  wire readmem;
  wire progcount;
  wire loadout;
  wire loadinst;
  wire loadb;
  wire loada;
  wire enableinstr;
  wire enablein;
  wire enablealu;
  wire enablea;
  wire addsub;

  // program counter
  // driven by pc
  wire [3:0] program;

  // memory data out
  // driven by mem  
  wire [7:0] memout;

  // accumulator output
  // driven by accumulators b and a
  wire [3:0] alub;
  wire [3:0] alua;

  phase U_phaseclk
  (
    .rstz (rst),
    .clk (clk),
    .phase (phaseclk)
  );

  control U_control
  (
    .phase (phaseclk),
    .instr (instruction),
    .readmem (readmem),
    .progcount (progcount),
    .load_out (loadout),
    .load_instr (loadinst),
    .load_b (loadb),
    .load_a (loada),
    .enable_instr (enableinstr),
    .enable_in (enablein),
    .enable_alu (enablealu),
    .enable_a (enablea),
    .addsub (addsub)
  );

  pc U_pc
  (
    .rst (rst),
    .clk (clk),
    .en (progcount),
    .count (program)
  );

  memory U_memory
  (
    .read (readmem),
    .write (writemem),
    .addr (program[2:0]),
    .data_in (memin),
    .data_out (memout)
  );

  ir U_ir
  (
    .rst (rst),
    .clk (clk),
    .enable_ir (enableinstr),
    .load_ir (loadinst),
    .instr_in (memout[7:4]),
    .data_in (memout[3:0]),
    .instr_out (instruction),
    .data_out (bus)
  );
  
  accb U_accb
  (
    .rst (rst),
    .clk (clk),
    .load_b (loadb),
    .b_in (bus),
    .alu_out (alub)
  );

  acca U_acca
  (
    .rst (rst),
    .clk (clk),
    .enable_a (enablea),
    .load_a (loada),
    .a_in (bus),
    .alu_out (alua),
    .bus (bus)
  );

  alu U_alu
  (
    .enable_alu (enablealu),
    .addsub (addsub),
    .a (alua),
    .b (alub),
    .carry (),
    .alu_out (bus)
  );

  in U_in
  (
    .enable (enablein),
    .data_in (din),
    .data_out (bus)
  );

  out U_out
  (
    .rst (1'b1),
    .clk (clk),
    .load (loadout),
    .data_in (bus),
    .data_out (out)
  );

endmodule

