`include "adder.v"

module tb_adder();

  reg a;
  reg b;
  reg ci;
  wire s;
  wire co;

  initial
  begin
  end

  initial
  begin
    a = 0;
    b = 0;
    ci = 0;

    #10 ci = 1;
    #10 ci = 0;
    b = 1;
    #10 ci = 1;
    #10 a = 1;
    b = 0;
    ci = 0;
    #10 ci = 1;
    #10 b = 1;
    ci = 0;
    #10 ci = 1;

    #50 $finish;
    
  end

  adder U_adder
  (
    .a (a),
    .b (b),
    .ci (ci),
    .s (s),
    .co (co) 
  );

  initial
  begin
    $dumpfile("out/tb_adder.lxt");
    $dumpvars(0, tb_adder);
  end

endmodule
