`include "acca.v"

module tb_acca();

  reg enable_a;
  reg rst;
  reg load_a;
  reg clk;
  reg [3:0] data;

  wire [3:0] alu;
  wire [3:0] bus;

  acca U_acca
  (
    .rst (rst),
    .clk (clk),
    .enable_a (enable_a),
    .load_a (load_a),
    .a_in (data),
    .alu_out (alu),
    .bus (bus)
  );

  initial
  begin
    rst <= 1;
    clk <= 0;
    enable_a <= 0;
    load_a <= 0;
    data = 4'b0;

    #5 rst = 0;
    #50 rst = 1;

    #15 data = 4'h5;
    load_a = 1;

    #10 load_a = 0;
    #10 data = 4'h0;
    #20 enable_a = 1;
    #20 enable_a = 0;

    #20 data = 4'hc;
    load_a = 1;

    #10 load_a = 0;
    #10 data = 4'h0;
    #20 enable_a = 1;
    #20 enable_a = 0;

    #50 $finish;
  end

  always
  begin
    #10 clk = ~clk;
  end

  initial
  begin
    $dumpfile("out/tb_acca.lxt");
    $dumpvars(0,tb_acca);
  end

endmodule  
